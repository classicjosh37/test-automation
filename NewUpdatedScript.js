var assert = require('assert'),
{Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
const args = process.argv[3];
describe('legalmatch', function() {
  let driver = new Builder().forBrowser('chrome').build();
  var source = driver.getPageSource('https://'+args+'.legalmatch.com');
  it('should go to legalmatch.com', function() {
    driver.get('https://'+args+'.legalmatch.com');
    driver.wait(until.titleIs('Find a Lawyer Near You | Find an Attorney you need - LegalMatch'),100000);
    driver.findElement(By.className('case-intake-form__input-button dropdown-toggle js-case-intake-category-input')).click();
  }),
  it('should check the page source of the website', function() {
    var source = driver.getPageSource('https://'+args+'.legalmatch.com');
    source.then(function (src) {
    console.log(source);
  });
  }),
  it('should check Title of the website', function() {
    var title = driver.getTitle('https://'+args+'.legalmatch.com');
    var expectedTitle = 'Find a Lawyer Near You | Find an Attorney you need - LegalMatch';
    title.then(function (title) {
     assert(title, expectedTitle);
    console.log(title);
  });
  }),
  it('should choose a category ', function() {
  var exist = driver.findElement(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]'));
  exist.then(function (exist) {
  driver.findElement(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]')).click();
  console.log('Category Exist!');
  });
}),
 it('should input a valid location', function() {
   var exist = driver.findElement(By.className('case-intake-form__field-item case-intake-form__input js-case-intake-location-input location-input'));
  exist.then(function (exist) {
  driver.findElement(By.className('case-intake-form__field-item case-intake-form__input js-case-intake-location-input location-input')).sendKeys('00001');
   console.log('Location Inputted!');
 });
}),
  it('should click submit button', function() {
  var exist = driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit'));
  exist.then(function (exist) {
  driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit')).click();
  console.log('Successfully Submitted!');
  });
});
});
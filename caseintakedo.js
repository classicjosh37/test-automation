var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
const args = process.argv[3];
describe('Sub-Category', function() {
  let driver = new Builder().forBrowser('chrome').build();
  var source = driver.getPageSource('https://'+args+'.legalmatch.com/home/caseIntake.do');
  it('should check the URL of the website', function() {
    var URL = driver.getCurrentUrl();
    var expectedUrl = 'https://'+args+'.legalmatch.com/home/caseIntake.do';
    URL.then(function (webUrl) {
    console.log('Website URL: ' +URL);
    expect(URL).to.eql(expectedUrl);
  });
  }),
  it('should choose Most Common Issues ', function() {
  //**you can use id in choosing most common issues**//
  //**chooses the Adoption common issues in family category**//
  var issues = driver.findElement(By.id('chosenCat_$leftId_0')).click();
  issues.then(function(webElement){
    console.log('Element exists on Common Issues');
  },function(err){
    if(err.state && err.state === 'no such element') {
    }else{
      console.log('Alert! Element not found on Common Issues!');
    }
  });
}),
 it('should click the next button', function() {
  var next = driver.findElement(By.className('next')).click();
  next.then(function(webElement){
    console.log('Element exists on next button');
  },function(err){
    if(err.state && err.state === 'no such element') {
    }else{
      console.log('Alert! Element not found on next button!');
    }
  });
}),
});
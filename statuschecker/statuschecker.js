var LineByLineReader = require('line-by-line'),
 lr = new LineByLineReader('url_list.txt');
const request = require('request');
const server = process.argv[2];

console.log(server);

const opts = {
    errorEventName:'error',
        //directory where you want to save your log file//
        logDirectory:'/Dockerfile', 
        fileNamePattern:'Test_Result-<DATE>.log',
        dateFormat:'YYYY.MM.DD'
};

const log = require('simple-node-logger').createRollingFileLogger( opts );

lr.on('line', function (line) {

    lr.pause();


   request(line, function (error, response) {

    var url = line;

    var url = url.replace("www", server);

    console.log('From line :', url);

        if (response && response.statusCode == 200) {

            console.log('[Test Passed] - Status Code: ',response && response.statusCode);

            log.info('\nFrom line : '+url+'\n');

            log.info('\nTest Passed : ',response.statusCode);

        }else{

            console.log('[Test Failed] - Status Code: ',response && response.statusCode);

            log.info('\nFrom line : '+url+'\n');

            log.info('\nTest Failed: ', error);
        }; 


         
    });

    setTimeout(function () {

        lr.resume();

    }, 2000);

});

lr.on('end', function () {
    console.log('Test Is Done!')
});

'use strict';
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
var mocha = require('mocha');
const args = process.argv[3];


module.exports = {
        
NewSix: function(driver){


it('should choose Sign Up', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

      });

    });

  }),

it('should Input LogIn Information username ', function() {


  var info1 = driver.wait(until.elementLocated(By.id('email'))).sendKeys('TestQA30000112345807@yahoo.com');

  info1.then(function(webElement){

    console.log('Element exists on Log In Information username');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Log In Information username!');

    }

  });
}),
it('should Input LogIn Information password ', function() {
  var info2 = driver.wait(until.elementLocated(By.id('password'))).sendKeys('TestQA3007');

  info2.then(function(webElement){

    console.log('Element exists on Log In Information password ');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Log In Information password!');

    }
  });
  }),
it('should Input LogIn Information confirm password ', function() {
  var info3 = driver.wait(until.elementLocated(By.id('validatePassword'))).sendKeys('TestQA3007');

  info3.then(function(webElement){

    console.log('Element exists on Log In Information confirm password ');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Log In Information confirm password!');

    }

  });

});
},

NewSixB: function(driver){
  
  it('should Input Clients Contact Information', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

  });

}),

  it('should input first name ', function() {


  var info1 = driver.wait(until.elementLocated(By.id('firstName'))).sendKeys('Robin');

  info1.then(function(webElement){

    console.log('Element exists on first name ');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on first name!');

    }

  });

}),

   it('should input last name ', function() {


  var info2 = driver.wait(until.elementLocated(By.id('lastName'))).sendKeys('Hood');

  info2.then(function(webElement){

    console.log('Element exists on last name ');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on last name!');

    }

  });

}),

   it('should input Day time phone ', function() {


  var info3 = driver.wait(until.elementLocated(By.id('daytimePhone'))).sendKeys('(555)123-4567');

  info3.then(function(webElement){

    console.log('Element exists on Day time phone');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Day time phone!');

    }

  });

}),
   it('should click next button ', function() {

    driver.wait(until.elementLocated(By.xpath('//*[@id="signup"]/div[7]/button'))).click();

   });

},

NewSixAlternative: function(driver){
  
  it('should go to legalmatch.com/home/registration/start.do', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

  });

}),

  it('should choose Log in ', function() {


  var info = driver.wait(until.elementLocated(By.xpath('//*[@id="content"]/strong/a'))).click();

  info.then(function(webElement){

    console.log('Element exists on start.do ');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on start.do!');

    }

  });

});

}

}
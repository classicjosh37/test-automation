'use strict';
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
var mocha = require('mocha');
const args = process.argv[3];


module.exports = {
        
NewFourA: function(driver){


it('should go to legalmatch.com/home/caseIntake4b.do', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

 });

 }),

  it('should Input Age of child/children', function() {
  //--you can use id in choosing most common issues--//
  //--chooses the Adoption common issues in family category--//
  var age = driver.wait(until.elementLocated(By.id('Ans:43_0'))).sendKeys('14');

  age.then(function(webElement){

    console.log('Element exists on Age');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Age!');

    }

  });

});
},

NewFourB: function(driver){

 it('should fill up Relationship to the child/children', function() {
  
  var relationship = driver.wait(until.elementLocated(By.id('Rdo:7_1'))).click();

  relationship.then(function(webElement){

    console.log('Element exists on relationship');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on relationship!');

    }

  });

});

},

NewFourC: function(driver){

 it('should fill up where the child/children are currently in custody ', function() {
  
  var custody = driver.wait(until.elementLocated(By.id('Rdo:8_1'))).click();

  custody.then(function(webElement){

    console.log('Element exists on custody');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on custody!');

    }

  });

});

},

NewFourD: function(driver){

 it('should fill up The biological mother of the child/children ', function() {
  
  var mother = driver.wait(until.elementLocated(By.id('Rdo:9_0'))).click();

  mother.then(function(webElement){

    console.log('Element exists on biological mother choices');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on biological mother choices!');

    }

  });

});

},

NewFourE: function(driver){

 it('should fill up The biological father of the child/children ', function() {
  
  var father = driver.wait(until.elementLocated(By.id('Rdo:10_0'))).click();

  father.then(function(webElement){

    console.log('Element exists on biological father choices');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on biological father choices!');

    }

  });

});

},

NewFourF: function(driver){

 it('should fill up Age of the client ', function() {
  
  var age = driver.wait(until.elementLocated(By.id('Ans:2823_0'))).sendKeys('44');

  age.then(function(webElement){

    console.log('Element exists on age of client');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on age of client!');

    }

  });

});

},

NewFourG: function(driver){

 it('should fill up Marital status ', function() {
  
  var marital = driver.wait(until.elementLocated(By.id('Rdo:3_1'))).click();

  marital.then(function(webElement){

    console.log('Element exists on marital status');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on marital status!');

    }

  });

});

},

NewFourH: function(driver){

 it('should fill up Occupation ', function() {
  
  var occupation = driver.wait(until.elementLocated(By.id('Ans:2815_0'))).sendKeys('Computer Technician');

  occupation.then(function(webElement){

    console.log('Element exists on occupation');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on occupation!');

    }

  });

});

},

NewFourI: function(driver){

 it('should fill up Gross annual income ', function() {
  
  var income = driver.wait(until.elementLocated(By.id('Rdo:892_0'))).click();

  income.then(function(webElement){

    console.log('Element exists on income');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on income!');

    }

  });

});

},

NewFourJ: function(driver){

 it('should click the next button ', function() {

  driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

 });
  
 driver.wait(until.elementLocated(By.xpath('//*[@id="questions"]/div[2]/button'))).click();


});

}

}
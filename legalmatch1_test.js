const {Builder, By, Key, until} = require('selenium-webdriver');
(async function legalmatch() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    //* go to legalmatch website *//
    await driver.get('https://qa3.legalmatch.com');
    //* click the Explore LegalMatch button at the upper right corner of the website *//
    await driver.findElement(By.className('header__nav-item explore-lm__toggle overlay__toggle js-overlay-toggle')).click();
    //* click the Family Button in the category list *//
    await driver.findElement(By.className('explore-lm__categories-link')).click();
    //* click the Adoption lawyers link *//
    await driver.findElement(By.linkText('Adoption lawyers')).click();
    //* click the LegalMatch logo to home *//
    await driver.findElement(By.className('svg-logo')).click();
    //* click the choose category drop down button *//
    await driver.findElement(By.className('case-intake-form__input-button dropdown-toggle js-case-intake-category-input')).click();
    //* click Family button from the dropdown list *//
    await driver.findElement(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]')).click();
    //* Inputed a valid zip code in the location text box *//
    await driver.findElement(By.name('location')).sendKeys('90210');
    //* click the Find My Lawyer button *//
    await driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit')).click();
    //* click the adoptions checkbox *//
    await driver.findElement(By.id('chosenCat_$leftId_0')).click();
    //* click the Guardian checkbox *//
    await driver.findElement(By.id('chosenCat_$rightId_1')).click();
    //* click the Paternity checkbox *//
    await driver.findElement(By.id('chosenCat_$rightId_3')).click();
    //* click the Next button *//
    await driver.findElement(By.className('next')).click();
  } finally {
    await driver.quit();
  }
})

();

'use strict';
var assert = require('assert'),
{Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
 
describe('GetStarted', function() {
  let driver = new Builder().forBrowser('chrome').build();
  const args = process.argv[3];
  it('should go to legalmatch.com', function() {
    driver.get('http://'+args+'.legalmatch.com');
    driver.findElement(By.className('case-intake-form__input-button dropdown-toggle js-case-intake-category-input')).click();
    driver.findElement(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]')).click();
    driver.findElement(By.name('location')).sendKeys('00001');
    driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit')).click();
 }),
  it('should go to caseIntake.do', function() {
    driver.get('http://'+args+'.legalmatch.com/home/caseIntake.do');
    driver.findElement(By.id('chosenCat_$leftId_0')).click();
    driver.findElement(By.className('next')).click();
   }),
   it('should go to caseIntake4b.do', function() {
    driver.get('http://'+args+'.legalmatch.com/home/caseIntake4b.do');
    driver.findElement(By.id('Ans:43_0')).sendKeys('14');
    driver.findElement(By.id('Rdo:7_0')).click();
    driver.findElement(By.id('Rdo:8_1')).click();
    driver.findElement(By.id('Rdo:9_1')).click();
    driver.findElement(By.id('Rdo:10_1')).click();
    driver.findElement(By.id('Ans:2823_0')).sendKeys('32');
    driver.findElement(By.id('Rdo:3_0')).click();
    driver.findElement(By.id('Rdo:892_0')).click();
   }),
    it('should go to caseIntake4a.do', function() {
    driver.findElement(By.id('summary')).sendKeys('Testing Testing Testing');
    driver.findElement(By.id('description')).sendKeys('Hello world hello world hello world');
    driver.findElement(By.className('next')).click();
   }),
     it('should go to start.do', function() {
    //**Sign Up or Log in**//
   }),
      it('should go to caseIntake.do', function() {
    //driver.findElement(By.className('case-intake-form__input-button dropdown-toggle js-case-intake-category-input')).click();
    //driver.findElement(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]')).click();
    //driver.findElement(By.name('location')).sendKeys('00001');
   // driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit')).click();
   });
});
